import { Component } from '@angular/core';
import { Hero } from './hero'
import { HeroService } from './hero.service';

@Component({
    selector: 'my-dashboard',
    templateUrl: './dashboard.html',
    styleUrls: [ './dashboard.component.css' ]
})

export class DashboardComponent {
    heroes: Hero[] = [];

    constructor(private HeroService: HeroService) { }

    ngOnInit() {
        this.HeroService.getHeroes().then(heroes => this.heroes = heroes.slice(1, 5));
    }

}